import { LogoutComponent } from './logout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardService } from './services/auth-guard.service';
import { ForbidenComponent } from './auth-guard/forbiden.component';
import { BoxPfaComponent } from './shared/pfa/box-pfa.component';

import { Routes } from '@angular/router';
import { PfaComponent } from './components/pfa/pfa.component';

export const AppRoutes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        canActivate: [AuthGuardService],
        children: [
            {
                path: 'home',
                canLoad: [AuthGuardService],
                loadChildren: './home/home.module#HomeModule'
            },
            {
                path: 'components',
                canLoad: [AuthGuardService],
                loadChildren: './components/components.module#ComponentsModule'
            },
        ]
    },
    {
        path: 'auth',
        canLoad: [AuthGuardService],
        loadChildren: './auth-guard/auth-guard.module#AuthGuardModule'
    },

];
