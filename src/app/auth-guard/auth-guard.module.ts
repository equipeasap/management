import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LogoutComponent } from "../logout.component";
import { SharedModule } from "../shared/shared.module";
import { AuthGuardRoutes } from "./auth-guard.routing";
import { ForbidenComponent } from "./forbiden.component";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AuthGuardRoutes),
        SharedModule,
    ],
    declarations: [
        ForbidenComponent,
        LogoutComponent
    ],
    exports: [

    ]
})

export class AuthGuardModule { }
