
import { Routes } from '@angular/router';
import { LogoutComponent } from '../logout.component';
import { ForbidenComponent } from './forbiden.component';

export const AuthGuardRoutes: Routes = [
  {
    path: 'forbiden',
    component: ForbidenComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  }
];
