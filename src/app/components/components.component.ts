import { AsapService, Global } from 'asap-crud';
import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html'
})

export class ComponentsComponent {

  public id;
  public type;
  public userID;
  public profile;
  public cargo;

  constructor (
    public route: ActivatedRoute,
    public service: AsapService,
    public global: Global
  ) {
    this.route.params.subscribe(params => {
      if (params.type) {
        this.type = params.type;
      }
    });
    
    this.cargo = this.global.loggedUser().value.cargos;

    this.global.setSessionData('getLevel', ()=> {
      const cargo = this.global.loggedUser().value.cargos;
      console.log('Cargo ->', cargo);
      return cargo;
    });
  }

  

}
