import { AsapService, Global } from 'asap-crud';
import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-skills-user',
  templateUrl: './skills-user.component.html',
  styleUrls: ['./skills-user.component.css']
})

export class SkillsUserComponent {

  public id;
  public type;
  public userID;
  public profile;
  public cargo;
  public listUsers;
  public formID;
  public userUnit;
  public ratingID;

  constructor(
    public route: ActivatedRoute,
    public service: AsapService,
    public global: Global
  ) {
    this.route.params.subscribe(params => {
      if (params) {
        console.log('params',params);
        this.ratingID = params.rating_id;
        this.service.basePath = "api/v2/client";
        this.service.entityName = "competence-rating/users/" + params.user_id;

        this.service.getResources().subscribe(response => {
          this.listUsers = response.data;
          // console.log('360', this.listUsers);
        });

      }
    });

    this.cargo = this.global.loggedUser().value.cargos;
    console.log("cargo", this.cargo);
    this.global.setSessionData('getLevel', () => {
      const cargo = this.global.loggedUser().value.cargos;
      console.log('Cargo ->', cargo);
      return cargo;
    });
  }

  public users = [
    { id: '120', name: 'Joao', check: true, cargo: 'Professor' , answers: false },
    { id: '120', name: 'Eduardo', check: false, cargo: 'Recepcionista', answers: false },
    { id: '120', name: 'Serafine', check: false, cargo: 'Professor', answers: false },
    { id: '120', name: 'Monica', check: true, cargo: 'Professor', answers: false},
    { id: '120', name: 'Bruna', check: true, cargo: 'Recepcionista', answers: false },
    { id: '120', name: 'Danilo', check: false, cargo: 'Recepcionista', answers: false },
    { id: '120', name: 'Paulo', check: false, cargo: 'Professor', answers: false },
    { id: '120', name: 'Neto', check: false, cargo: 'Professor', answers: false }
  ];



}
