import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { CompetenceRatingService } from './../../services/competence-rating.service';
import { CompetenceCategoryService } from './../../services/competence-category.service';
import { CompetenceSurveyService } from './../../services/competence-survey.service';
import { Global, AsapService } from 'asap-crud';
import { Component, ɵSWITCH_TEMPLATE_REF_FACTORY__POST_R3__ } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-skills',
    templateUrl: './skills.component.html',
    providers: [AsapService, CompetenceSurveyService, CompetenceCategoryService, CompetenceRatingService]
})

export class SkillsComponent {

    @BlockUI() blockUI: NgBlockUI;

    public id;
    public user;
    public answer = {};
    public survey;
    public rating: any;
    public cargo;
    public answered;
    public media;
    public unidade;
    public existeMedia;
    public loading;
    public userID;
    public ratingID;
    public userInfo;
    public mediaGestor;
    public mediaColab;
    public feedBack;
    
    

    constructor(
        public global: Global,
        public service: AsapService,
        public route: ActivatedRoute,
        public surveyService: CompetenceSurveyService,
        public categoryService: CompetenceCategoryService,
        public ratingService: CompetenceRatingService
    ) {
        this.user = this.global.loggedUser();
        console.log("user",this.user.value.cargos);
        this.route.params.subscribe(params => {
            if (params) {
                this.ratingID = params.rating_id;
                this.userID = params.user_id;
                this.surveyService.getSurveyByRating(params.user_id,params.rating_id).subscribe(response => {
                    this.survey = response.data;  
                    this.userInfo = response.userInfo;  
                    this.getAnswer(params.rating_id);                
                });
            }
        });    
        
    }

    ngOnInit() {
        //this.blockUI.start('Loading...'); 
    }

    public getUserData(id) {
        this.service.basePath = "api/v2/admin";
        this.service.entityName = "asap-user/dados";
        this.service.getResource(id).subscribe(response => {
            // console.log('User ->', response[0]);
            this.user = response[0];
        });
    }

    public sendAnswer() {
        let data = {            
            evaluator_id: this.global.loggedUser().id,
            evaluated_id: this.userInfo.id,
            answers: this.answer

        }
        this.loading = true;
        //this.blockUI.start('Loading...'); 
        console.log("data",data);
        this.surveyService.answer(data).subscribe(response => {
            if (response.success) {
                if (this.global.alert('Avaliação', 'Respondindo com sucesso!', 'success')) {
                    // location.reload();
                }
                this.getAnswer(this.ratingID);
                // this.getCategory(this.id);
                this.loading = false;               
            }
        });
        
        //this.blockUI.stop();
        //this.global.stopWaiting();
    }

    public getCategory(ratingID) {
        this.survey = null;
        // this.surveyService.getSurveyByRating(ratingID).subscribe(response => {
        //     this.survey = response.data;
        //     console.log("[getCategory] survey -> ", this.survey);
        // });
        console.log(" [getCategory] survey 2 -> ", ratingID);
        // console.log(this.user);
        // if (this.user.extras) {
        //     this.user.extras = this.user.extras.map(item => {
        //         if ((item.value) && (item.value != '')) {
        //             //item.value = JSON.parse((item.value));                    
        //             item.value = JSON.parse((JSON.stringify(item.value)));
        //         }
        //         return item;
        //     });                  
        // } 
        //console.log(this.user.value.unidades);
        // this.getMedia(ratingID,this.user.client_id,this.user.value.unidades);
        // this.getAnswer(this.user.id, ratingID);
    }

    public getAnswer(ratingID) {
        // this.answer = null;
        this.surveyService.getAnswer(this.userInfo.id,ratingID).subscribe(response => {
            this.answer = response.data;
            if (Object.keys(this.answer).length) {
                this.answered = true;
            }
            else {
                this.answered = false;
            }
            this.getMedia(ratingID,this.userInfo.id);
        });
    }   

    public doAnswer(id,ans) {
        if (!this.answered) {
            this.answer[id] = ans;
        }
    }

    public getMedia(ratingID,evaluatedID) {
        this.surveyService.getMedia(ratingID,evaluatedID).subscribe(response => {
            console.log("media",response);
            this.mediaGestor = response.mediaGestor;
            this.mediaColab = response.mediaColab;
            if (this.mediaGestor > 0 && this.mediaColab > 0) {
                this.existeMedia = true;                
                this.getFeedBack(evaluatedID,this.global.loggedUser().id,ratingID);
            }
            else {
                this.existeMedia = false;
            }
        });
    }

    public sendFeedBack(feedback) {

        let data = {            
            evaluator_id: this.global.loggedUser().id,
            evaluated_id: this.userInfo.id,
            feedback: feedback,
            rating_id: this.ratingID 

        }
        this.loading = true;
        //this.blockUI.start('Loading...'); 
        console.log("data",data);
        this.surveyService.sendFeedBack(data).subscribe(response => {
            if (response.success) {
                if (this.global.alert('Feedback', 'Enviado com sucesso!', 'success')) {
                    // location.reload();
                }
                // this.getAnswer(this.ratingID);
                this.getFeedBack(data.evaluated_id,data.evaluator_id,data.rating_id);
                this.loading = false;               
            }
        });

    }

    public getFeedBack(evaluatedID,evaluatorID,ratingID) {
        this.surveyService.getFeedBack(evaluatedID,evaluatorID,ratingID).subscribe(response => {
            console.log("feedback",response);
            this.feedBack = response.feedback;
        });
    }

    // public rating = [

    //     { type: 'alternative', number: '1', question: 'Está disponível para ajudar a unidade?', complement: '(Realizar coberturas, efetuar trocas ou ficar além do seu horário, no limite de sua disponibilidade).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '2', question: 'Ausência de trabalhos e faltas?', complement: '(Exceto casos atípicos ou motivo justo como doença, óbito).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '3', question: 'Está presente em todas as reuniões e participa ativamente?', complement: '(Comparece no horário e interage com a equipe, exceto casos atípicos ou motivo justo, doença, óbito).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '4', question: 'Aplica o PAIS 100%?', complement: '(Se apresenta ao cliente, faz anamnese, preenche a ficha de treino por completo, realiza treinos dentro dos 10 minutos, realiza intervenções, as abordagens são pontuais e assertivas, falado NPS).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '5', question: 'Aplica o AVAS 100%?', complement: '(Se apresenta ao cliente, oferece para conhecer, lança 100% das visitas, segue o script de vendas, segue as normas da smart Fit, está disponível, sabe ouvir, resolve problemas).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '6', question: 'Entrega alto padrão?', complement: '(Atencioso, solicito, educado, cordial, assertivo, dinâmico, boa postura, prioriza assuntos pertinentes ao trabalho).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '7', question: 'É proativo?', complement: '(se antecipa, ajuda em processos fora da função, resolve problemas quando o líder não está).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '8', question: 'Estabelece relações harmônicas com a equipe?', complement: '(Proporciona um clima agradável na relação interpessoal).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '9', question: 'Colaborador aceita feedbacks?', complement: '(Entende e repeita as opiniões alheias e procura evoluir).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '10', question: 'Assume a reponsabilidade pelos resultados da unidades?', complement: '(NPS).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

    //     { type: 'alternative', number: '11', question: 'É o melhor professor/recepcionista da unidade?', complement: '(De acordo com as necessidades da Smart Fit e as questões acima).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },


    //     { type: 'rating', number: 'x', question: 'Exemplo para utilização de sistema rating', complement: 'Complemento da pergunta, caso sistemas rating.', ratingName: 'rating-1', zeroStar: 'a-0', oneStar: 'a-1', twoStars: 'a-2', threeStars: 'a-3', fourStars: 'a-4', fiveStars: 'a-5' },

    // ];

}
