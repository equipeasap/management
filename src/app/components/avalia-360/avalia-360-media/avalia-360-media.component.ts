import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-avalia-360-media',
    templateUrl: './avalia-360-media.component.html'
    //   styleUrls: ['./avalia-360-media.component.scss']
})
export class Avalia360MediaComponent implements OnInit {

    @Input() inputFormID = null;
    @Input() inputUserID = null;
    public evaluation;
    public id;
    public formID;
    public questions;
    public userInfo;

    constructor(
        public global: Global,
        public service: MembersService,
        public route: ActivatedRoute

    ) {

  
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params) {

                this.service.basePath = "api/v2/admin";
                if (this.inputUserID && this.inputFormID) {
                    this.service.entityName = "feedback/360/media/" + this.inputFormID + '/' + this.inputUserID;
                }
                else if (params.form_id && params.user_id) {
                    this.service.entityName = "feedback/360/media/" + params.form_id  + '/' + params.user_id;
                    this.formID = params.form_id;
                }

                this.service.getResources().subscribe(response => {
                    this.questions = response.questions;
                    this.userInfo = response.userInfo;
                });
               
            }
        });

    }

    public users = [
        { id: '120', name: 'Joao', check: true, cargo: 'Professor' },
        { id: '120', name: 'Eduardo', check: false, cargo: 'Recepcionista' },
        { id: '120', name: 'Serafine', check: false, cargo: 'Professor' },
        { id: '120', name: 'Monica', check: true, cargo: 'Professor' },
        { id: '120', name: 'Bruna', check: true, cargo: 'Recepcionista' },
        { id: '120', name: 'Danilo', check: false, cargo: 'Recepcionista' },
        { id: '120', name: 'Paulo', check: false, cargo: 'Professor' },
        { id: '120', name: 'Neto', check: false, cargo: 'Professor' }
      ];

}
