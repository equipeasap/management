import { Component, OnInit, Input } from '@angular/core';
import { Global, AsapService } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatGridTileHeaderCssMatStyler } from '@angular/material';


@Component({
    selector: 'app-avalia-360-report',
    templateUrl: './avalia-360-report.component.html'
    //   styleUrls: ['./avalia-360-relatory.component.scss']
})
export class Avalia360ReportComponent implements OnInit {

    public evaluation;
    public newUserID;
    public oldUserID;
    public folderID;
    public formID;
    public queryParams = '';
    public arrayDataNavigate = [];

    constructor(
        public global: Global,
        public service: AsapService,
        public route: ActivatedRoute,
        private router: Router,


    ) {
        this.service.basePath = "api/v2/admin";

        // this.route.params.subscribe(params => {
        //   if (params) {
        //     this.id = params.id;
        //     // this.getData();
        //   }
        //   else {
        //     if (this.regionalId) {
        //       this.getData();

        //     }
        //   }
        // });
    }

    ngOnInit() {
        // console.log("1dsadsa",this.regionalId);
        this.queryParams = '';

        this.route.params.subscribe(params => {
            console.log('params',params);
            this.evaluation = null;

            if (params) {
                this.formID = params.form_id;
                this.folderID = params.sub_folder_id;
                this.newUserID = params.new_user_id;
                // this.oldUserID = params.old_user_id;
                this.getData();
             
            }
            else {              
                this.getData();
            }
        });

    }

    public getData() {
        
        if (this.newUserID) {                        
            this.service.entityName = "feedback/report/user/360/" + this.folderID + '/' + this.formID + '?user=' + this.newUserID;
        }
        // else if (this.oldUserID){
        //     this.service.entityName = "feedback/report/user/360/" + this.folderID + '/' + this.formID + '?user=' + this.oldUserID;
        // }
        
        let params: any = {};
        // params.query = "user=" + this.id;
        

        this.service.createResource({}).subscribe(response => {
            console.log('RESPONSE DATA ->', response);
            this.evaluation = response.data;
            this.navigate(this.evaluation.user.id,this.evaluation.user.name);
        })
    }

    public navigate(userID, name) {
        const dataNavigate = {
            user_id: userID,
            name: name,
            form_id: this.formID,
            folder_id: this.folderID
        };
        console.log('[navigate]', dataNavigate);
        this.global.getOrCreateSessionData('avalia.360.navigate',[]).push(dataNavigate);
        // this.router.navigate(['components/avalia-360/relatory', this.folderID, this.formID, userID]);

    }

}
