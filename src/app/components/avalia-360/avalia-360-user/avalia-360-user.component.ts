import { AsapService, Global } from 'asap-crud';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
    selector: 'app-avalia-360-user',
    templateUrl: './avalia-360-user.component.html'
})

export class Avalia360UserComponent implements OnInit{

    public id;
    public type;
    public userID;
    public profile;
    public cargo;
    public questions;
    public userInfo;
    public answers = {};
    public formID;
    public answered;

    public starValue = [
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        // Conheço
    ];

    constructor(
        public route: ActivatedRoute,
        public service: AsapService,
        public global: Global,
        private router: Router
    ) {
      

        // this.cargo = this.global.loggedUser().value.cargos;
        // this.userID = this.global.loggedUser().id;

        // this.global.setSessionData('getLevel', ()=> {
        //   const cargo = this.global.loggedUser().value.cargos;
        //   console.log('Cargo ->', cargo);
        //   return cargo;
        // });
    }
    ngOnInit(): void {
        console.log('[Avalia360UserComponent] Carregou');

        this.route.params.subscribe(params => {
            if (params) {
                console.log('1');
                this.service.basePath = "api/v2/admin";
                this.service.entityName = "feedback/360/form/" + params.form_id + '/' + params.user_id;
                this.formID = params.form_id;
                this.type = params.type;
                this.service.getResources().subscribe(response => {
                    this.questions = response.questions;
                    this.userInfo = response.userInfo;                    
                    this.getAnswer(params.form_id);
                });
            }
        });
    }

    public starIndex(value) {
        if (!value) return 0;
        return this.starValue.indexOf(value);
    }

    public doAnswer(id, ans) {
        this.answers[id] = ans;
    }

    public sendAnswer() {
        this.service.basePath = "api/v2/admin";
        this.service.entityName = "feedback/360/answer";
        let data = {
            evaluator_id: this.global.loggedUser().id,
            evaluated_id: this.userInfo.id,
            answers: this.answers
        };
        console.log("data",data);
        this.service.createResource(data).subscribe(response => {
            if (response.success) {
                if (this.global.alert('Avaliação', 'Respondinda com sucesso!', 'success')) {
                }
                this.getAnswer(this.formID);                
            }
        });
    }

    public getAnswer(formID) {
        this.service.basePath = "api/v2/admin";
        this.service.entityName = "feedback/360/answer/" + this.userInfo.id + '/' + formID;
        this.service.getResources().subscribe(response => {
            this.answers = response.data;
            // console.log()
            if (Object.keys(this.answers).length) {
                this.answered = true;
            }
            else {
                this.answered = false;
            }            
        });
    }

    public clearNavigate() {
        this.global.removeSessionData('avalia.360.navigate');
        this.router.navigate(['/']);
    }


    public rating = [

        // { type: 'alternative', number: '1', question: 'Está disponível para ajudar a unidade?', complement: '(Realizar coberturas, efetuar trocas ou ficar além do seu horário, no limite de sua disponibilidade).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '2', question: 'Ausência de trabalhos e faltas?', complement: '(Exceto casos atípicos ou motivo justo como doença, óbito).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '3', question: 'Está presente em todas as reuniões e participa ativamente?', complement: '(Comparece no horário e interage com a equipe, exceto casos atípicos ou motivo justo, doença, óbito).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '4', question: 'Aplica o PAIS 100%?', complement: '(Se apresenta ao cliente, faz anamnese, preenche a ficha de treino por completo, realiza treinos dentro dos 10 minutos, realiza intervenções, as abordagens são pontuais e assertivas, falado NPS).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '5', question: 'Aplica o AVAS 100%?', complement: '(Se apresenta ao cliente, oferece para conhecer, lança 100% das visitas, segue o script de vendas, segue as normas da smart Fit, está disponível, sabe ouvir, resolve problemas).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '6', question: 'Entrega alto padrão?', complement: '(Atencioso, solicito, educado, cordial, assertivo, dinâmico, boa postura, prioriza assuntos pertinentes ao trabalho).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '7', question: 'É proativo?', complement: '(se antecipa, ajuda em processos fora da função, resolve problemas quando o líder não está).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '8', question: 'Estabelece relações harmônicas com a equipe?', complement: '(Proporciona um clima agradável na relação interpessoal).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '9', question: 'Colaborador aceita feedbacks?', complement: '(Entende e repeita as opiniões alheias e procura evoluir).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '10', question: 'Assume a reponsabilidade pelos resultados da unidades?', complement: '(NPS).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },

        // { type: 'alternative', number: '11', question: 'É o melhor professor/recepcionista da unidade?', complement: '(De acordo com as necessidades da Smart Fit e as questões acima).', yes: 'Sim', no: 'Não', maybe: 'Às vezes' },


        { type: 'rating', number: '1', description: 'Exemplo para utilização de sistema rating', orientation: 'Complemento da pergunta, caso sistemas rating.', ratingName: 'rating-1', zeroStar: 'a-0', oneStar: 'a-1', twoStars: 'a-2', threeStars: 'a-3', fourStars: 'a-4', fiveStars: 'a-5' },
        { type: 'rating', number: '2', description: 'É o melhor professor/recepcionista da unidade', orientation: 'Complemento da pergunta, caso sistemas rating.', ratingName: 'rating-2', zeroStar: 'a-0', oneStar: 'a-1', twoStars: 'a-2', threeStars: 'a-3', fourStars: 'a-4', fiveStars: 'a-5' },
        { type: 'rating', number: '3', description: 'Assume a reponsabilidade pelos resultados da unidades?', orientation: 'Complemento da pergunta, caso sistemas rating.', ratingName: 'rating-3', zeroStar: 'a-0', oneStar: 'a-1', twoStars: 'a-2', threeStars: 'a-3', fourStars: 'a-4', fiveStars: 'a-5' },

    ];



}
