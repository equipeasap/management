import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Global, AsapService } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatGridTileHeaderCssMatStyler } from '@angular/material';


@Component({
    selector: 'app-avalia-360-breadcrumb',
    templateUrl: './avalia-360-breadcrumb.component.html'
    //   styleUrls: ['./avalia-360-relatory.component.scss']
})
export class Avalia360BreadcrumbComponent implements OnInit {

    public arrayNavigate:any = [];

    constructor(
        public global: Global,
        public service: AsapService,
        public route: ActivatedRoute,
        private router: Router,
    ) {
       
    }

    ngOnInit() {
        // this.arrayNavigate = JSON.parse(sessionStorage.getItem('avalia.360.navigate'));
        this.arrayNavigate = this.global.getOrCreateSessionData('avalia.360.navigate',[]);
        console.log("arrayNavigate 1", this.arrayNavigate);
       
    }

    public navigateBack(index) {
        let item = this.arrayNavigate[index];
        this.arrayNavigate.splice(index);     
        console.log("[navigateBack] arrayNavigate ",this.arrayNavigate, item);   
        if (item.user_id) {
            this.router.navigate(['/components','avalia-360', item.folder_id, item.form_id, item.user_id, item.type]);
        }
        else {
            this.router.navigate(['/components','avalia-360', item.folder_id, item.form_id, item.type, item.key, item.data]);
        }
    }

    public clearNavigate() {
        this.arrayNavigate.splice(0,this.arrayNavigate.length);
        console.log("arrayNavigate 2", this.arrayNavigate);

        this.router.navigate(['/']);
    }

 


}
