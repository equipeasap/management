import { AsapService, Global } from 'asap-crud';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: 'app-avalia-360',
    templateUrl: './avalia-360.component.html',
    styleUrls: ['../../home/home.component.scss', './avalia-360.component.scss' ]
})

export class Avalia360Component implements OnInit {

    public id;
    public type;
    public userID;
    public profile;
    public cargo;
    public listUsers;
    public formID;
    public userUnit;
    public isEnable;
    public loading = null;
    public avalia360Dados;
    public folderID;
    public evaluation;
    public key = null;
    public data = null;
    public evaluationList: any;
    public lastPage: any = 0;
    public currentPage: any = 1;

    constructor(
        public route: ActivatedRoute,
        public service: AsapService,
        public global: Global
    ) {
  
    }
    ngOnInit(): void {
        this.route.params.subscribe(params => {
            if (params) {
                this.formID = params.form_id;
                this.type = params.type;
                this.folderID = params.sub_folder_id;
                this.userID = params.user_id;
               
                this.service.basePath = "api/v2/admin";
                this.service.entityName = "feedback/360/" + params.sub_folder_id + '/' + params.form_id;    
                       
                if (params.key) {
                    this.key = params.key;                    
                }
                if (params.data) {
                    this.data = params.data;                                    
                }
                this.service.getResources().subscribe(response => {
                    this.listUsers = response.data;  
                    this.isEnable = response.isEnable;                  
                });
                this.evaluation = null;
                this.getData();

            }
        });
        console.log("teste",this.global.loggedUser().id);
        // this.userID = this.global.loggedUser().id;
        this.cargo = this.global.loggedUser().value.cargos;
        console.log("cargo", this.cargo);
        this.global.setSessionData('getLevel', () => {
            const cargo = this.global.loggedUser().value.cargos;
            console.log('Cargo ->', cargo);
            return cargo;
        });
    }

    public getData() {
        
        if (this.userID) {                        
            this.service.entityName = "feedback/report/user/360/" + this.folderID + '/' + this.formID + '?user=' + this.userID + '&key=' + this.key + '&data=' + this.data;
        }
        else {
            this.service.entityName = "feedback/report/user/360/" + this.folderID + '/' + this.formID + '?user=' + null + '&key=' + this.key + '&data=' + this.data;

        }
        // else if (this.oldUserID){
        //     this.service.entityName = "feedback/report/user/360/" + this.folderID + '/' + this.formID + '?user=' + this.oldUserID;
        // }
        
        let params: any = {};
        // params.query = "user=" + this.id;
        

        this.service.createResource({}).subscribe(response => {
            
            console.log('RESPONSE DATA ->', response);
            this.evaluation = response.data;
            this.navigate(this.evaluation.user.id,this.evaluation.user.name);

            if (this.evaluation.list.current_page && this.evaluation.list.last_page) {
                this.lastPage = response.data.list.last_page;
                this.currentPage = response.data.list.current_page;
            }
        })
    }


    public navigate(userID, name) {
        const dataNavigate = {
            user_id: userID,
            name: name,
            form_id: this.formID,
            folder_id: this.folderID,
            type: this.type,
            key: this.key,
            data: this.data
        };
        console.log('[navigate]', dataNavigate);
        this.global.getOrCreateSessionData('avalia.360.navigate',[]).push(dataNavigate);
        // this.router.navigate(['components/avalia-360/relatory', this.folderID, this.formID, userID]);

    }

    public loadMore() {
        this.currentPage++;
        this.loading = true;
        this.service.createResource({},{query: "&page="+this.currentPage }).subscribe(response => {
            // console.log('RESPONSE DATA 1 ->', response);
            // console.log("[loadMore]",this.evaluation);
 
            for (let item of response.data.list.data) {
                this.evaluation.list.data.push(item);
            }
        
            this.lastPage = response.data.list.last_page;
            this.currentPage = response.data.list.current_page;
            this.loading = false;
            // console.log("[loadMore] 2",this.evaluation);

        });
    }


}
