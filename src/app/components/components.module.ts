import { LeaderNewComponent } from './pfa/leader-new/leader-new.component';
import { RegionalNewComponent } from './pfa/regional-new/regional-new.component';
import { RegionalSeniorNewComponent } from './pfa/regional-senior-new/regional-senior-new.component';
import { Avalia360MediaComponent } from './avalia-360/avalia-360-media/avalia-360-media.component';
import { Avalia360ReportComponent } from './avalia-360/avalia-360-report/avalia-360-report.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ComponentsRoutes } from './components.routing';

import { ComponentsComponent } from './components.component';

import { SkillsComponent } from './skills/skills.component';
import { MapComponent } from './map/map.component';
import { PfaComponent } from './pfa/pfa.component';
import { RegionalLeaderComponent } from './pfa/regional-leader/regional-leader.component';
import { RegionalLeaderDataComponent } from './pfa/regional-leader/regional-leader-data/regional-leader-data.component';
import { RegionalSeniorComponent } from './pfa/regional-senior/regional-senior.component';
import { RegionalSeniorDataComponent } from './pfa/regional-senior/regional-senior-data/regional-senior-data.component';
import { UnitLeaderComponent } from './pfa/unit-leader/unit-leader.component';
import { UnitLeaderDataComponent } from './pfa/unit-leader/unit-leader-data/unit-leader-data.component';

import { AsapSkeletonModule } from 'asap-skeleton';
import { AsapImagePipeModule } from 'asap-image-pipe';
import { RegionalSeniorListComponent } from './pfa/regional-senior/regional-senior-list/regional-senior-list.component';
import { ManagementLeaderComponent } from './map/management-leader/management-leader.component';
import { AsapUploadModule } from 'asap-upload';
import { FirebaseModule } from '../firebase.module';
import { BlockUIModule } from 'ng-block-ui';
import { Avalia360Component } from './avalia-360/avalia-360.component';
import { Avalia360UserComponent } from './avalia-360/avalia-360-user/avalia-360-user.component';
import { SkillsUserComponent } from './skills/skills-user/skills-user.component';
import { UserNewComponent } from './pfa/user-new/user-new.component';
import { Avalia360BreadcrumbComponent } from './avalia-360/avalia-360-breadcrumb/avalia-360-breadcrumb.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(ComponentsRoutes),
    AsapSkeletonModule,
    AsapImagePipeModule,
    AsapUploadModule,
    FirebaseModule,
    SharedModule,
    BlockUIModule.forRoot()
  ],
  declarations: [
    ComponentsComponent,
    SkillsComponent,
    SkillsUserComponent,
    MapComponent,
    PfaComponent,
    // 
    RegionalLeaderComponent,
    RegionalLeaderDataComponent,
    // 
    RegionalSeniorComponent,
    RegionalSeniorDataComponent,
    RegionalSeniorListComponent,
    RegionalSeniorNewComponent,
    RegionalNewComponent,
    LeaderNewComponent,
    UserNewComponent,

    // 
    UnitLeaderComponent,
    UnitLeaderDataComponent,
    // 
    ManagementLeaderComponent,
    //
    Avalia360Component,
    Avalia360UserComponent,
    Avalia360ReportComponent,
    Avalia360MediaComponent,
    Avalia360BreadcrumbComponent
  ],
  exports: [
    AsapImagePipeModule
  ]
})

export class ComponentsModule { }
