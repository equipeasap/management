import { LeaderNewComponent } from './pfa/leader-new/leader-new.component';
import { RegionalSeniorNewComponent } from './pfa/regional-senior-new/regional-senior-new.component';
import { RegionalNewComponent } from './pfa/regional-new/regional-new.component';
import { Avalia360MediaComponent } from './avalia-360/avalia-360-media/avalia-360-media.component';
import { PfaComponent } from './pfa/pfa.component';

import { Routes } from '@angular/router';
import { ComponentsComponent } from './components.component';
import { RegionalLeaderDataComponent } from './pfa/regional-leader/regional-leader-data/regional-leader-data.component';
import { RegionalSeniorDataComponent } from './pfa/regional-senior/regional-senior-data/regional-senior-data.component';
import { RegionalSeniorListComponent } from './pfa/regional-senior/regional-senior-list/regional-senior-list.component';
import { ManagementLeaderComponent } from './map/management-leader/management-leader.component';
import { MapComponent } from './map/map.component';
import { UnitLeaderDataComponent } from './pfa/unit-leader/unit-leader-data/unit-leader-data.component';
import { SkillsComponent } from './skills/skills.component';
import { Avalia360Component } from './avalia-360/avalia-360.component';
import { Avalia360UserComponent } from './avalia-360/avalia-360-user/avalia-360-user.component';
import { SkillsUserComponent } from './skills/skills-user/skills-user.component';
import { UserNewComponent } from './pfa/user-new/user-new.component';
import { Avalia360ReportComponent } from './avalia-360/avalia-360-report/avalia-360-report.component';

export const ComponentsRoutes: Routes = [
  {
    path: ':type',
    children: [
      {
        path: '',
        component: ComponentsComponent
      }
    ]
  },
  // {
  //   path:'pfa/regional/:id',
  //   component: RegionalLeaderDataComponent
  // },
  // {
  //   path:'pfa/senior/:id',
  //   component: RegionalSeniorDataComponent
  // },
  {
    path:'map/:id',
    component: MapComponent
  },
  {
    path: 'pfa/:folderId',
    component: PfaComponent
  },
  {
    path: 'pfa/regional/:folderId/:id',
    component: RegionalNewComponent
  },
  {
    path: 'pfa/regional/:folderId/:id/:regionalSeniorId',
    component: RegionalNewComponent
  },
  {
    path: 'pfa/regional/senior/user/:folderId/:id',
    component: RegionalSeniorNewComponent
  },
  {
    path: 'pfa/regional/lider/:folderId/:id/:regionalId/:regionalSeniorId',
    component: LeaderNewComponent
  },
  {
    path: 'pfa/regional/lider/user/comum/data/:folderId/:id/:regionalId',
    component: UserNewComponent,
  },
  {
    path: 'pfa/regional/lider/user/:folderId/:id/:leaderId/:regionalId/:regionalSeniorId',
    component: UserNewComponent
  },
 
  // {
  //   path:'pfa/senior/list/:id',
  //   component: RegionalSeniorListComponent
  // },
  // {
  //   path:'pfa/senior/list/:regional_id/:id',
  //   component: RegionalSeniorDataComponent
  // },
  // {
  //   path:'management/:type/:user/:id',
  //   component: ManagementLeaderComponent
  // },
  // {
  //   path:'pfa/leader/:id',
  //   component: UnitLeaderDataComponent
  // },
  {
    path:'skills/rating/:user_id/:rating_id',
    component: SkillsComponent
  },
  {
    path:'skills/:rating_id/:user_id',
    component: SkillsUserComponent
  },
  {
    path:'avalia-360/:sub_folder_id/:form_id/:user_id/:type',
    component: Avalia360Component
  },

  {
    path:'avalia-360/:sub_folder_id/:form_id/:type/:key/:data',
    component: Avalia360Component
  },
  {
    path:'avalia-360/user/questions/answer/:form_id/:user_id/:type',
    component: Avalia360UserComponent
  },
  {
    path:'avalia-360/user/detail/:form_id/:user_id',
    component: Avalia360MediaComponent
  },
  {
    path:'avalia-360/report/:sub_folder_id/:form_id/:new_user_id',
    component: Avalia360ReportComponent
  }


];
