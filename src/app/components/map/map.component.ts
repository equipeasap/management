import { Global, AsapService } from "asap-crud";
import { Component, Input, SimpleChanges, OnChanges } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from "rxjs";
import * as moment_ from "moment";
const moment = moment_;

@Component({
    selector: "app-map",
    templateUrl: "./map.component.html",
    providers: [AsapService]
})
export class MapComponent {
    @Input() type;

    public loading = false;
    public user;
    public courses;
    public id;

    constructor(
        public global: Global,
        public service: AsapService,
        public route: ActivatedRoute,
    ) {
        this.service.basePath = "api/v21/admin";
        this.service.entityName = "trail/management";
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params) {
                this.id = params.id;
                this.getUserData(this.id);
                this.selectUser(this.id);
            }
            else {
                this.selectUser(this.global.loggedUser());
            }
        });
    }

    public selectUser(user = null) {
        console.log('USER ID', user);
        this.service.basePath = "api/v21/admin";
        this.service.entityName = "trail/management";
        let params: any = {};
        this.loading = true;
        if (user) {
            this.user = user;
            this.courses = null;
            params.query = "user=" + user
        }        
        this.service.getResources(params).subscribe((response) => {
            console.log(response);
            this.courses = response.map((course) => {
                if (course.feedback.length == 0) {
                    course.feedback.push({});
                }
                course.indicador = course.indicador.filter(i => i.value).map((i) => {
                    if (i.resp.length == 0) {
                        i.resp.push({});
                    }
                    return i;
                })

                course.notEvaluated = course.indicador.filter(i => {
                    if (!i.resp[0].resp) {
                        return true;
                    }
                    return false;
                });

                course.challenges = course.indicador.filter(i => {
                    if ((i.resp[0].resp == 'Falta prática') || (i.resp[0].resp == 'Preciso de suporte')) {
                        if ((!course.meta) && (i.resp[0].meta)) {
                            course.meta = i.resp[0].meta;
                        }
                        return true;
                    }
                    return false;
                });

                course.status = {
                    label: "Avaliando",
                    icon: "fa fa-edit",
                    color: "warning",
                    feedback: "Autoavaliação em execução"
                };
                if (course.notEvaluated.length == 0) {
                    course.status = {
                        label: "Avaliado",
                        icon: "fa fa-check-square-o",
                        color: "success",
                        feedback: "Autoavaliação realizada"
                    };
                }
                else if (course.notEvaluated.length == course.indicador.length) {
                    course.status = {
                        label: "Não Avaliado",
                        icon: "fa fa-ban",
                        color: "dark",
                        feedback: "Autoavaliação não iniciada"
                    };
                }

                return course;
            });
            this.loading = false;
            console.log("selectUser", this.courses);
        });
    }

    public getUserData(id) {
        this.service.basePath = "api/v2/admin";
        this.service.entityName = "asap-user/dados";

        this.service.getResource(id).subscribe( response => {
            // console.log('User ->', response[0]);
            this.user = response[0];
        })
    }
}
