import { AngularFirestore } from '@angular/fire/firestore';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AsapService, Global } from 'asap-crud';
import { forkJoin } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as moment_ from 'moment';
const moment = moment_;

@Component({
    selector: 'app-management-leader',
    templateUrl: './management-leader.component.html',
    styleUrls: ['./management-leader.component.scss']
})
export class ManagementLeaderComponent {

    public type = 'leader';
    public id;
    public userID;

    public user;
    public loading: boolean = true; 
    public isValid;
    public cargo;

    public meta = null;

    public courses;

    public starValue = [
        'Não marcado',
        'Não conheço',
        'Preciso de suporte',
        'Falta prática',
        'Moderado',
        'Avançado',
        // Conheço
    ];

    public skills;

    constructor(
        public global: Global,
        public service: AsapService,
        public route: ActivatedRoute,
        public db: AngularFirestore
    ) {
        this.user = this.global.loggedUser();
        this.route.params.subscribe((params) => {
            console.log("paramss", params);
            if (params.id) {
                this.id = params.id;
            }
            if (params.type) {
                this.type = params.type;
            }
            if (params.user) {
                this.userID = params.user;
            }

            //   if (this.type == "user") {
            //     this.user = this.global.loggedUser();
            //     this.userID = this.user.id;
            //     this.selectUser();
            //   }
            if (this.id && this.userID) {
                this.selectUser(this.userID);
                this.selectCourse(this.id, this.userID);
            }
            console.log(this.id, this.type, this.userID);
        });
        console.log(this.starValue[4]);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.type) {
            if (this.type == "user") {
                this.user = this.global.loggedUser();
                this.userID = this.user.id;
                //this.selectUser();
            }
        }
        if (this.id && this.userID) {
            this.selectUser(this.userID);
            this.selectCourse(this.id, this.userID);
        }
    }

    public selectUser(userID) {
        
        this.service.basePath = "api/v2/admin";
        this.service.entityName = "asap-user/dados";

        this.service.getResource(userID).subscribe((response) => {
            console.log("selectUser", response);
            this.user = response[0];      
        });
    }

    public canEdit() {
        this.user = this.global.loggedUser();
        // console.log("canEdit", this.user);
        // if (this.type == 'user') {

        //     return true;
        // }
        // else if (this.type == 'leader' && this.userID == this.user.id) {
        //     return false;
        // }

        // return false;
        // console.log("this.user",this.user.value.cargos);
        if (this.user.value.cargos == 'regional' || this.user.value.cargos == 'lider') {        
            return false;            
        }
        else {            
            return true;
        }
    }

    public canEditFeedback() {
        this.user = this.global.loggedUser();
        // console.log("canEdit", this.user);
        // if (this.type == 'user') {

        //     return true;
        // }
        // else if (this.type == 'leader' && this.userID == this.user.id) {
        //     return false;
        // }

        // return false;
        if (this.user.value.cargos == 'regional' || this.user.value.cargos == 'lider') {
            ///console.log("xdddddddd");
            return true;
            
        }
        else {
            //console.log("xddddddddaaaa");
            return false;
        }
    }

    public selectCourse(id, userID) {
        this.service.basePath = "api/v21/admin";
        this.service.entityName = "trail/management";

        this.loading = true;
        let params: any = {};

        params.query = "user=" + userID;
        console.log("params", params);

        this.service.getResource(id, params).subscribe((response) => {
            console.log("selectCourse 1", response);
            this.courses = response.map((course) => {
                if (course.feedback.length == 0) {
                    course.feedback.push({});
                }
                course.indicador = course.indicador.filter(i => i.value).map((i) => {
                    if (i.resp.length == 0) {
                        i.resp.push({});
                    }
                    return i;
                })
                return course;
            });
            this.loading = false;
            //console.log("selectCourse", this.courses);
        });
    }

    public challenges() {
        let challenge = [];
        if (this.courses) {
            //console.log("challenges", this.courses);
            this.courses.map((course) => {
                course.indicador.map((item) => {
                    // if (item.resp[0].length == 0) {
                    //     this.isValid = false;
                    // }                    
                    if ((item.resp[0].resp == 'Falta prática') || (item.resp[0].resp == 'Preciso de suporte')) {
                        if ((item.resp[0].meta) && (!this.meta)) {
                            this.meta = item.resp[0].meta;
                        }
                        challenge.push(item);
                        return item;
                    }
                    //this.isValid 
                    return false;
                });
                return false;
            });
        }
        return challenge;


    }

    public getAllAnswers() {
        let answer = [];
        if (this.courses) {
            //console.log("answer 1",this.courses);
            this.courses.map((course) => {
                course.indicador.map((item) => {
                    //console.log("item 1", item.resp[0].resp);
                    if (item.resp[0].resp) {
                        answer.push(item);
                        this.isValid = true;
                        return item;
                    }
                    else {
                        this.isValid = false;
                    }
                    return false;
                });
                return false;
            });
            return false;
        }
        //console.log("answer", answer);
        return answer;

    }

    public sendEvaluation(course) {
        let requests:any = [];
        this.loading = true;
        this.global.startWaiting("Enviando avaliação!");
        this.service.entityName = "trail/management";
        console.log("course =>", course);
        for (let indicator of course.indicador) {
            // for (let resp of indicator.resp)
            if (indicator.resp[0].resp) {
                indicator.resp[0].indicador_id = indicator.id;
                indicator.resp[0].meta = this.meta;
                console.log("indicador =>", indicator.resp[0]);
                if (indicator.resp[0].id) {
                    requests.push(this.service.updateResource(indicator.resp[0]));
                }
                else {
                    requests.push(this.service.createResource(indicator.resp[0]));
                }
            }
        }

        if (requests.length > 0) {
            let data = {};
            forkJoin(requests).subscribe((response) => {
                //console.log("sendEvaluation",response);
                console.log("sendEvaluation",requests[0].source.source.source.value.method);
                if (requests[0].source.source.source.value.method == 'PUT') {
                    data = {
                        user: this.global.loggedUser(),
                        title: 'Atualizou sua Autoavaliação!',
                        message: 'Meta: ' + this.meta ? this.meta : 'Não definida',
                        status: 'avaliado',
                        created_at: Date.now(),
                        updated_at: Date.now()
                    }
                }
                else {
                    data = {
                        user: this.global.loggedUser(),
                        title: 'Realizou uma Autoavaliação!',
                        message: 'Meta: ' + this.meta ? this.meta : 'Não definida',
                        status: 'avaliado',
                        created_at: Date.now(),
                        updated_at: Date.now()
                    }
                }
                

                // this.sendNotification(data);
                console.log("sendEvaluation", response);
                this.global.alert("Avaliação", "Avaliação enviada com sucesso!", "success");
                this.loading = false;
            });
        }


        this.global.stopWaiting();
    }

    public sendFeedback(course) {
        this.global.startWaiting("Enviando feedback!");
        this.service.entityName = "trail/feedback";
        if (course.feedback[0].feedback) {
            course.feedback[0].trail_id = course.id;
            if (course.feedback[0].id) {
                this.service.updateResource(course.feedback[0], { router: '' }).subscribe((response) => {
                    console.log("sendFeedback", response);
                    this.global.alert("Feedback", "Feedback enviado com sucesso!", "success");
                });
            }
            else {
                this.service.createResource(course.feedback[0]).subscribe((response) => {
                    console.log("sendFeedback", response);
                    this.global.alert("Feedback", "Feedback enviado com sucesso!", "success");
                });
            }
        }
        this.global.stopWaiting();
    }

    public uploadFile(event: any) {
        console.log('EVENT ->', event);
        this.courses.file = event.path;
    }

    public starIndex(value) {
        if (!value) return 0;
        return this.starValue.indexOf(value);
    }

    // public sendNotification(data) {
    //     const collection = this.db.collection('management/feedback/notifications');
    //     collection.add(data).then((response) => {
    //        console.log("sendNotification",response);
    //     });
    // }

}
