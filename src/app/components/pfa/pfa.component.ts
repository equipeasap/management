import { Global } from 'asap-crud';
import { Component } from '@angular/core';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pfa',
  templateUrl: './pfa.component.html',
  styleUrls: ['./../../home/home.component.scss']
})

export class PfaComponent {
    public permission;
    public permissionData;
    public leaderData;
    public userData;
    public regionalId;
    public cargo;

    constructor(
        public global: Global, 
        public service: MembersService,
        public route: ActivatedRoute
    ){
        this.getUserLevel();
        this.getUserPermission();
    }

    public getUserLevel() {
        this.service.getResources().subscribe( response => {
            console.log('RESPONSE ->', response);
            this.userData = response.data.user_autenticado[0];
        })
    }
  
    public getUserPermission() {
        this.service.entityName = 'feedback/page/controller';
        this.service.getResources().subscribe(response => {
            console.log('getUserPermission ->', response);
            this.permission = (response.data.userFeed.cargo)? response.data.userFeed.cargo : 'user';
            this.permissionData = response.data;
            // this.permission = 'senior';
            this.regionalId = response.data.userFeed.id;
            console.log("sdsadsadasd",this.regionalId);
            sessionStorage.setItem('permission', this.permission);
            // this.getLeaderData();
        });
  
    }

    public getLeaderData() {
        if (this.permission === 'regional') {
            this.service.entityName = 'feedback/page/reg';
            
            this.service.getResources().subscribe( response => {
                console.log('regional ->', response);
                this.leaderData = response;
            })
        }
        else if (this.permission === 'senior') {
            this.service.entityName = 'feedback/page/sen';

            this.service.getResources().subscribe( response => {
                console.log('senior ->', response);
                this.leaderData = response;
            })
        }
        else if (this.permission === 'adm') {
            this.service.entityName = 'feedback/page/adm';

            this.service.getResources().subscribe( response => {
                console.log('adm ->', response);
                this.leaderData = response;
            })
        }

        // else if (this.permission === 'lider') {
        //     this.service.entityName = 'feedback/page/lider';

        //     this.service.getResources().subscribe( response => {
        //         console.log('lider ->', response);
        //         this.leaderData = response;
        //     });
        // }
    }
    public pfa = [
        { question: 'Meu gerente me oferece um feedback prático que me ajuda a melhorar meu desempenho', qtd: '0', MdAnterior: '10', MdAtual: '10', MdGlobal: '10' },

        { question: 'Meu gerente permite que eu tome decisões no meu dia a dia, pois confia que eu estou alinhado com a empresa', qtd: '0', MdAnterior: '10', MdAtual: '10', MdGlobal: '10' },

        { question: 'Meu gerente demonstra consideração por mim como pessoa', qtd: '0', MdAnterior: '10', MdAtual: '10', MdGlobal: '10' },

        { question: 'Meu gerente mantem a equipe focada nos resultados prioritários', qtd: '0', MdAnterior: '10', MdAtual: '10', MdGlobal: '10' },

        { question: 'Meu gerente teve uma conversa importante comigo nos últimos 6 meses sobre o desenvolvimento de minha carreira', qtd: '0', MdAnterior: '10', MdAtual: '10', MdGlobal: '10' },

        { question: 'Meu gerente comunica objetivos claros a nossa equipe	', qtd: '0', MdAnterior: '10', MdAtual: '10', MdGlobal: '10' },
        { question: 'Meu gerente tem conhecimento necessário para gerenciar o meu trabalho com eficácia', qtd: '0', MdAnterior: '10', MdAtual: '10', MdGlobal: '10' },
        { question: 'Eu recomendaria meu gerente a outras pessoas da empresa', qtd: '0', MdAnterior: '10', MdAtual: '10', MdGlobal: '10' }
    ];

}
