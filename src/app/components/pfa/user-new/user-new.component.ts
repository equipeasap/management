import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-user-new',
    templateUrl: './user-new.component.html',
    styleUrls: ['./../../../home/home.component.scss']
})
export class UserNewComponent implements OnInit {

    @Input() id;
    public evaluation;
    // public id;
    public users;
    public permission = null;
    public regionalId;
    public regionalSeniorId;
    public leaderId;
    public userNameLeader;
    public userNameRegional;
    public userNameSenior;
    public folderId;

    constructor(
        public global: Global,
        public service: MembersService,
        public route: ActivatedRoute

    ) {
        this.permission = sessionStorage.getItem('permission');
    }

    ngOnInit() {

        this.route.params.subscribe(params => {
        console.log("params",params);
            if (params.folderId) {
                this.folderId = params.folderId;
            }
            if (params.id) {
                this.id = params.id;
                this.regionalId = params.regionalId;
                this.regionalSeniorId = params.regionalSeniorId;
                this.leaderId = params.leaderId;

                this.getUsernameSenior(this.regionalSeniorId);
                this.getUsernameRegional(this.regionalId);
                this.getUsernameLeader(this.leaderId);

                this.getData();
            }
            else if (this.id) {
                this.getData();
            }
        });
    }

    public getData() {

        if (this.id && this.folderId) {
            this.service.entityName = "feedback/page?user=" + this.id + "&folder_id=" + this.folderId;
        }
        this.service.getResources().subscribe(response => {
            this.evaluation = response.data;
        });

    }

    public getUsernameSenior(userId) {
        if (this.folderId) {
            this.service.entityName = 'feedback/page?user=' + userId + "&folder_id=" + this.folderId;
            this.service.getResources().subscribe(response => {
                this.userNameSenior = response.data.user[0].name;          
            });
        }
       
    }

    public getUsernameRegional(userId) {
        if (this.folderId) {
            this.service.entityName = 'feedback/page?user=' + userId + "&folder_id=" + this.folderId;
            this.service.getResources().subscribe(response => {
                this.userNameRegional = response.data.user[0].name;          
            });
        }
    }

    public getUsernameLeader(userId) {
        if (this.folderId) {
            this.service.entityName = 'feedback/page?user=' + userId + "&folder_id=" + this.folderId;
            this.service.getResources().subscribe(response => {
                this.userNameLeader = response.data.user[0].name;          
            });
        }
     
    }

 
}
