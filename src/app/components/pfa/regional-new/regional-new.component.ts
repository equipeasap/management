import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-regional-new',
    templateUrl: './regional-new.component.html',
    styleUrls: ['./../../../home/home.component.scss']
})
export class RegionalNewComponent implements OnInit {

    @Input() id;
    public evaluation;
    public regionalId;
    public users;
    public permission = null;
    public regionalSeniorId;
    public userName;
    public folderId;

    constructor(
        public global: Global,
        public service: MembersService,
        public route: ActivatedRoute

    ) {
        this.permission = sessionStorage.getItem('permission');
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            console.log("params 2", params);
            if (params.folderId) {
                this.folderId = params.folderId;
            }
            if (params.id) {
                this.id = params.id;

          
                if (params.regionalId) {
                    this.regionalId = params.regionalId;
                }
                else {
                    this.regionalId = params.id;
                }                

                this.regionalSeniorId = params.regionalSeniorId;
                this.getUsername(this.regionalSeniorId);
                this.getData();
            }           
            else if (this.id) {
                this.getData();
            }
           
        });

    }

    public getData() {
        if (this.id && this.folderId) {
            this.service.entityName = "feedback/page?user=" + this.id + '&folder_id=' + this.folderId;
        }
        
        this.service.getResources().subscribe(response => {
            console.log('RESPONSE DATA ->', response);
            this.evaluation = response.data;
        });
        this.getUsers();
    }

    public getUsers() {
        if (this.id && this.folderId) {            
            this.service.entityName = "feedback/page/reg?user=" + this.id + '&folder_id=' + this.folderId;
            this.service.getResources().subscribe(response => {
                this.users = response.data;
            });
        }
    }
    
    public getUsername(userId) {
        this.service.entityName = 'feedback/page?user=' + userId + '&folder_id=' + this.folderId;
        this.service.getResources().subscribe(response => {
            console.log('XD', response);  
            this.userName = response.data.user[0].name;          
        });
    }
}
