import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-regional-senior-new',
    templateUrl: './regional-senior-new.component.html',
    styleUrls: ['./../../../home/home.component.scss']
})
export class RegionalSeniorNewComponent implements OnInit {

    @Input() id;

    public evaluation;
    public users;
    public permission = null;
    public folderId;
    public filters;
    public type: string;
    public field: string;
    public selectedValue;
      

    constructor(
        public global: Global,
        public service: MembersService,
        public route: ActivatedRoute

        
    ) {
        this.permission = sessionStorage.getItem('permission');
    }

   
    ngOnInit() {
        this.route.params.subscribe(params => {
            console.log("params", params);    
            if (params.folderId) {
                this.folderId = params.folderId;
            }
            if (params.id) {
                this.id = params.id;
                this.getData();
            }

            else if (this.id) {
                this.getData();
            }
        });
    }

    public getData() {
        let query = "folder_id=" + this.folderId;

        this.service.entityName = "feedback/page";

        if (this.id) {
            query += "&user=" + this.id;
        }

        if (this.selectedValue) {
            query += "&" + this.selectedValue.type + "=" + this.selectedValue.field;
        }
        
        this.service.getResources({ query }).subscribe(response => {
            console.log('RESPONSE DATA ->', response);
            this.evaluation = response.data;
            if (!this.filters) {
                this.filters = response.data.filter;
            }

        });
        this.getUsers();

    }

    public getUsers() {
        if (this.id && this.folderId) {
            this.service.entityName = "feedback/page/reg?user=" + this.id + '&folder_id='+ this.folderId;
            this.service.getResources().subscribe(response => {
                this.users = response.data;
            });
        }
        
    }

    public clearFilter() {
        this.selectedValue = null;
        this.getData();
    }
 
}


