import { Component, OnInit } from '@angular/core';
import { MembersService } from 'src/app/services/members.service';
import { Global } from 'asap-crud';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-regional-leader-data',
  templateUrl: './regional-leader-data.component.html',
  styleUrls: ['./regional-leader-data.component.scss']
})
export class RegionalLeaderDataComponent implements OnInit {


  public id;
  public evaluation;
  public permission;

  constructor(
    public global: Global, 
    public service: MembersService,
    public route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      if (params) {
        this.id = params.id;
      }
    });
   }

  ngOnInit() {
    this.getData();
    this.permission = sessionStorage.getItem('permission');
  }

  public getData() {
    this.service.entityName = "feedback/page"

    this.service.getResources({ query: 'user=' + this.id }).subscribe( response => {
      console.log('RESPONSE DATA ->', response);
      this.evaluation = response.data;
    })
  }

}
