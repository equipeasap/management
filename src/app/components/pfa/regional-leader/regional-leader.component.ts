import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-regional-leader',
  templateUrl: './regional-leader.component.html',
  styleUrls: ['./regional-leader.component.scss']
})
export class RegionalLeaderComponent implements OnInit, OnChanges {

  @Input() data:any;
  public permission;

  constructor() { 
    
  }

  ngOnChanges(changes:SimpleChanges) {
    
  }


  ngOnInit() {
    console.log('Regiona Data Input', this.data);
   
    this.permission = sessionStorage.getItem('permission');
  }

}
