import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-unit-leader-data',
  templateUrl: './unit-leader-data.component.html',
  styleUrls: ['./unit-leader-data.component.scss']
})
export class UnitLeaderDataComponent implements OnInit {
  
  @Input() regionalId;
  public evaluation;
  public id;

  constructor(
    public global: Global, 
    public service: MembersService,
    public route: ActivatedRoute

  ) {

    // this.route.params.subscribe(params => {
    //   if (params) {
    //     this.id = params.id;
    //     // this.getData();
    //   }
    //   else {
    //     if (this.regionalId) {
    //       this.getData();

    //     }
    //   }
    // });
  }

  ngOnInit() {
    // console.log("1dsadsa",this.regionalId);
    
    this.route.params.subscribe(params => {
      if (params) {
        this.id = params.id;
        this.getData();
      }
      else {
          this.getData();
      }
    });
    
  }

  public getData() {
    // console.log("1",this.regionalId);
    if (this.regionalId) {
      console.log("2",this.regionalId);
      this.service.entityName = "feedback/page?user=" + this.regionalId;
    }
    else {
      this.service.entityName = "feedback/page";

    }
    let params:any = {};
    // params.query = "user=" + this.id;

    this.service.getResources().subscribe( response => {
      console.log('RESPONSE DATA ->', response);
      this.evaluation = response.data;
    })
  }

}
