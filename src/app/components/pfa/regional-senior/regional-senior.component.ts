import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-regional-senior',
  templateUrl: './regional-senior.component.html',
  styleUrls: ['./regional-senior.component.scss']
})
export class RegionalSeniorComponent implements OnInit {

  @Input() permission;
  @Input() data;

  constructor() { }

  ngOnInit() {
    console.log('Permission', this.permission);
    console.log('Data', this.data);
  }

}
