import { Component, OnInit } from '@angular/core';
import { MembersService } from 'src/app/services/members.service';
import { Global } from 'asap-crud';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-regional-senior-list',
  templateUrl: './regional-senior-list.component.html',
  styleUrls: ['./regional-senior-list.component.scss']
})
export class RegionalSeniorListComponent implements OnInit {


  public leaderData;
  public id;

  constructor(
    public global: Global, 
    public service: MembersService,
    public route: ActivatedRoute
  ){
    this.route.params.subscribe(params => {
      if (params) {
        this.id = params.id;
      }
    });
  }

  ngOnInit() {
    this.getLeadersData();
  }

  public getLeadersData() {
    this.service.entityName = 'feedback/page/sen';
    let params:any = {};
    params.query = "user=" + this.id;


    this.service.getResources(params).subscribe( response => {
        console.log('regional ->', response);
        this.leaderData = response;
    });
  } 

}
