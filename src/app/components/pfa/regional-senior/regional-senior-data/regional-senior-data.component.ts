import { Component, OnInit } from '@angular/core';
import { Global } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-regional-senior-data',
  templateUrl: './regional-senior-data.component.html',
  styleUrls: ['./regional-senior-data.component.scss']
})
export class RegionalSeniorDataComponent implements OnInit {

  public id;
  public regionalID;
  public evaluation;

  constructor(
    public global: Global, 
    public service: MembersService,
    public route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      if (params) {
        this.id = params.id;
        this.regionalID = params.regional_id;
      }
    });
   }

  ngOnInit() {
    this.getData();
  }

  public getData() {
    this.service.entityName = "feedback/page"

    this.service.getResources({ query: 'user=' + this.id }).subscribe( response => {
      console.log('RESPONSE DATA ->', response);
      this.evaluation = response.data;
    })
  }

}
