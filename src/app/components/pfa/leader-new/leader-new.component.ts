import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'asap-crud';
import { MembersService } from 'src/app/services/members.service';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-leader-new',
    templateUrl: './leader-new.component.html',
    styleUrls: ['./../../../home/home.component.scss']
})
export class LeaderNewComponent implements OnInit {

    // @Input() regionalId;
    public evaluation;
    public id;
    public users;
    public permission = null;
    public regionalId;
    public regionalSeniorId;
    public userNameSenior;
    public userNameRegional;
    public folderId;

    constructor(
        public global: Global,
        public service: MembersService,
        public route: ActivatedRoute

    ) {
        this.permission = sessionStorage.getItem('permission');
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params.folderId) {
                this.folderId = params.folderId;
            }
            if (params.id) {
                this.id = params.id;            
                this.regionalId = params.regionalId;
                this.regionalSeniorId = params.regionalSeniorId;

                this.getUsernameRegional(this.regionalId);
                this.getUsernameSenior(this.regionalSeniorId);

                this.getData();
            }
            // else if (this.regionalId) {
            //     this.getData();
            // }
        });

    }

    public getData() {
        if (this.id && this.folderId) {
            this.service.entityName = "feedback/page?user=" + this.id + "&folder_id=" + this.folderId;
        }

        this.service.getResources().subscribe(response => {
            console.log('RESPONSE DATA ->', response);
            this.evaluation = response.data;
            this.getUsers();

        });
    }

    public getUsers() {

        this.service.entityName = "feedback/page/reg?user=" + this.id + "&folder_id=" + this.folderId;
        this.service.getResources().subscribe(response => {
            this.users = response.data;
        });        
    }

    public getUsernameSenior(userId) {
        this.service.entityName = 'feedback/page?user=' + userId + "&folder_id=" + this.folderId;;
        this.service.getResources().subscribe(response => {
            this.userNameSenior = response.data.user[0].name;          
        });
    }

    public getUsernameRegional(userId) {
        this.service.entityName = 'feedback/page?user=' + userId + "&folder_id=" + this.folderId;;
        this.service.getResources().subscribe(response => {
            this.userNameRegional = response.data.user[0].name;          
        });
    }
}
