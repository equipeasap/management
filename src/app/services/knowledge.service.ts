import { AsapBaseService } from 'asap-crud';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class KnowledgeService extends AsapBaseService {
 
	constructor(public http: HttpClient) {
		super(http);
		this.basePath = "api/v2/client";
		this.entityName = "management";
    }

    public getKnowledgeByUser(userID) {
        return this.getResources({router:'knowledge/' + userID});
    } 
    
    public getKnowledgeProgress(userID) {
        return this.getResources({router:'knowledge/progress/' + userID}); 
    }    
    
  
}
