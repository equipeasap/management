import { Injectable } from '@angular/core';
import { AsapBaseService } from 'asap-crud';
import { Global } from 'asap-crud';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AsapClientPipe } from '../core/pipes/asap-client.pipe';

@Injectable()
export class ThemeService extends AsapBaseService {

    public theme;
    public themeLoaded;
    public clientNotFound = false;

    constructor(
        public http: HttpClient,
        public global: Global,
        public router: Router
    ) {
        super(http);
        this.basePath = 'api/v2';
        this.entityName = '';
    }

    public dictionary() {
        return AsapClientPipe.dictionary;
    }

    public useEAD() {
        if ((this.theme.layout == 1) || (this.theme.layout == 4)) {
            return true;
        }
        return false;
    }

    public usePortal() {
        if (this.theme.layout == 5) {
            return true;
        }
        else if (this.theme.layout == 4) {
            if (!this.dictionary().templateType) {
                return true;
            }
            else if (this.dictionary().templateType == 'full') {
                return true;
            }
            else if ((this.global.loggedUser()) && (this.dictionary().tastingUsers.indexOf(this.global.loggedUser().email) >= 0)) {
                return true;
            }
        }
        else {
            return false;
        }
        return false;    
    }

}
