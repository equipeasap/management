import { Injectable } from '@angular/core';
import { AsapBaseService } from 'asap-crud';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MembersService extends AsapBaseService {

  constructor(public http: HttpClient) {
    super(http);
    this.basePath = "api/v2/client";
    this.entityName = "gamefication/pontos";
  }
}
