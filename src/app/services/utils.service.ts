import { Injectable } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseApp } from '@angular/fire';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public percentage: Observable<number>;
  public task: AngularFireUploadTask;
  public snapshot: Observable<any>;
  public lastDoc;

  constructor(
    public firebaseStorage: AngularFireStorage,
    public db: AngularFirestore,
    public firebase: FirebaseApp,
  ) { }

  public uploadFile(fileName: string, base64: string) {
    console.log('TO SUBINDO', fileName);
    try {
      let correctBase64 = base64;
      // if (base64.indexOf('data:image') > -1 ) {
      //   correctBase64 = base64.split(',')[1];
      // }
      const ref = this.firebaseStorage.ref(fileName);
      this.task = ref.putString(correctBase64, 'data_url');
      this.percentage  = this.task.percentageChanges();
      this.snapshot = this.task.snapshotChanges();
    } catch (error) {
      console.log(`ERRO DO SUBINDO`, error);
    }
  }

  getFireStorageURL(id:string) {
    return this.firebaseStorage.ref(id).getDownloadURL();
  }

  async deleteFireStorage(id:string) {
    return await this.firebaseStorage.ref(id).delete().toPromise();
  }

  uid() {
    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
      this.s4() + '-' + this.s4() + this.s4() + this.s4();
  }

  s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  isUrl(s: string): boolean {
    const regexp = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    return regexp.test(s);
  }

}
