import { Injectable, OnInit } from '@angular/core';
import { Router, CanActivate, ActivatedRoute, CanLoad, Route, UrlSegment } from '@angular/router';
import { AuthService } from './auth.service';
import { Global } from 'asap-crud';
import { Observable } from 'rxjs';

declare var swal: any;

@Injectable()
export class AuthGuardService implements CanLoad, CanActivate {

    constructor(
        public global: Global,
        public auth: AuthService,
        public router: Router,
        public route: ActivatedRoute
    ) {
        console.log('[AuthGuardService]',new Date())
    }

    canLoad(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
        // console.log('CanLoad:', segments);
        // console.log('CanLoad QueryString:', this.auth.params);
        // console.log('CanLoad LoggedUser:', this.global.loggedUser());

        if (this.global.loggedUser()) {
            if (['forbiden'].indexOf(route.path) >= 0) {
                console.log('[AuthGuardService] CanLoad A');
                console.log('[AuthGuardService] NavigateHome A');
                this.router.navigate(['/home']);
                return false;
            }
            return true;
        } 
        else {
            if (this.auth.params) {
                console.log('[AuthGuardService] CanLoad B');
                console.log('[AuthGuardService] Params A');
                const brand = this.auth.params.brand;
                if (this.auth.params.token) {
                    console.log('[AuthGuardService] CanLoad C');
                    return new Promise((resolve) => {
                        this.auth.setSessionToken({token:this.auth.params.token}).then(
                            (response) => {
                                localStorage.setItem('client',this.global.loggedUser().client_id);
                                let route = segments.map((item) => item.path);
                                this.router.navigate(route);
                                resolve(true);
                            },
                            (error) => {
                                console.log('[AuthGuardService] forbiden A');
                                this.router.navigate(['/auth/forbiden']);
                                resolve(false);
                            }

                        );
                    });
                }
            } 
            else {
                console.log('[AuthGuardService] CanLoad D', route);
                if (['forbiden'].indexOf(route.path) >= 0) {
                    return true;
                }
                console.log('[AuthGuardService] CanLoad E', route);
                console.log('[AuthGuardService] forbiden B', route);
                this.router.navigate(['/auth/forbiden']);
                return false;
            }
        }
    }

    canActivate(): Observable<boolean> | boolean {
        const clientId = sessionStorage.getItem('client');
        // console.log('canActivate:', this.router.url);
        // console.log('canActivate QueryString:', this.auth.params);
        console.log('[AuthGuardService] canActivate A');

        if (this.global.loggedUser()) {
            if (['forbiden'].indexOf(this.router.url) >= 0) {
                console.log('[AuthGuardService] canActivate B');            
                this.router.navigate(['/home']);
            }
            console.log('[AuthGuardService] canActivate C');
            return true;
        }
        else if ((this.auth.params) && (this.auth.params.token)) {
            console.log('[AuthGuardService] canActivate D');
            return true;
        }
        else {
            if (['/auth/forbiden','/'].indexOf(this.router.url) >= 0) {
                console.log('[AuthGuardService] canActivate E');
                return true;
            }
            console.log('[AuthGuardService] canActivate F');
            return false;
        }
        // const verify = this.auth.verifyClient(+clientId).map(res => {

        //     console.log('AuthGuard.CanActivate: Verify ', res);

        //     if (res.success) {
        //         if (['active-directory', 'login', 'recovery'].indexOf(this.router.url) >= 0) {
        //             this.router.navigate(['/home']);
        //         }
        //         return true;
        //     } else {
        //         this.router.navigate(['/erros/403']);
        //     }
        // }).first();

        // return verify;
    }

    errorNotificate(msg: string): any {
        swal({
            title: 'Erro',
            text: msg,
            type: 'warning',
            confirmButtonClass: 'btn btn-success',
        });
    }
}
