import { AsapBaseService } from 'asap-crud';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CompetenceSurveyService extends AsapBaseService {

	constructor(public http: HttpClient) {
		super(http);
		this.basePath = "api/v2/client";
		this.entityName = "competence-survey";
    }
    
    public answer(data) {
        return this.createResource(data,{router:'answer'});
    }

    public getAnswer(userID,ratingID) {
        return this.getResources({router:'answer/' + userID + '/' + ratingID});
    }

    public getSurveyByRating(userID,ratingID) {
        return this.getResources({router:'rating/' + userID + '/' + ratingID});
    }

    public getMedia(ratingID,evaluatedID) {
        return this.getResources({router:'media/' + ratingID + '/' + evaluatedID });
    }

    public sendFeedBack(data) {
        return this.createResource(data,{router:'send/feedback'});
    }

    public getFeedBack(evaluatedID,evaluatorID,raitingID) {
        return this.getResources({router:'feedback/' + evaluatedID + '/' + evaluatorID + '/' + raitingID});
    }
}
