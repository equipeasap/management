import { AsapBaseService } from 'asap-crud';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService extends AsapBaseService {

    public params:any = null;

    constructor(public http: HttpClient) {
        super(http);
        this.basePath = 'api';
        this.entityName = 'v2'
    }

    public getUserLogged() {
        return this.getResources({router: 'client/user/authenticated'});
    }

    public verify() {
        return this.getResources({router: 'client/verifica'});
    }

    public setSessionToken(data): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const token = 'Bearer' + ' ' + data.token;
            sessionStorage.setItem('token', token);
            setTimeout(() => {
                this.getUserLogged().subscribe((response: any) => {
                    this.verify().subscribe(verifica => {
                        if (response.success === false) {
                            resolve(false);
                        } else {
                            sessionStorage.setItem('loggedUser', JSON.stringify(response));
                            resolve(true);
                        }
                    });
                }, err => reject(err));
            }, 100);
        });
    }

}