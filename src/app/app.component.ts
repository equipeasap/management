import { AuthService } from './services/auth.service';
import { MembersService } from './services/members.service';
import { ActivatedRoute, Router, RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { Global, AsapService } from 'asap-crud';
import { Component } from "@angular/core";

import { CLIENT_DATA } from "./clients";
import { environment } from 'src/environments/environment';
import { SharedModule } from './shared/shared.module';

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    providers: [MembersService]
})
export class AppComponent {
    public title = "asap-management";
    public CLIENT: string = "smartfitLiderRegional";
    public profile;
    public permission;
    public leaderData;

    constructor(
        public global: Global,
        public service: MembersService,
        public route: ActivatedRoute,
        public auth: AuthService,
        public router: Router
    ) {

    }

    ngOnInit(): void {
        sessionStorage.setItem(
            "streamURL",
            "https://fitnesslink.twcreativs.stream/"
        );
        sessionStorage.setItem("apiURL", environment.apiURL);        
        
        this.router.events.subscribe((event: RouterEvent) => {
            this.navigationInterceptor(event)
        });
    }

    public getQueryParams(route) {
        let uri = route.split("?");
        if (uri.length == 1) {
            return null;
        }
        else {
            let result: any = {}
            uri = uri[1];
            uri = uri.split("&");
            for (let params of uri) {
                let paramIdx = params.indexOf("=");
                let key = params.substring(0, paramIdx);
                let value = params.substring(paramIdx + 1);
                result[key] = decodeURIComponent(value);
            }
            return result;
        }
    }

    public navigationInterceptor(event: RouterEvent): void {
        
        if (event instanceof NavigationStart) { 
            let token = null;
            console.log("[navigationInterceptor] NavigationStart A", event);
            
            this.auth.params = this.getQueryParams(event.url);
            if (this.auth.params && this.auth.params.token) {
                token = this.auth.params.token;
            }

            if (!token && this.global.loggedUser()) { 
                if (event.url == '/') {
                    this.router.navigate(['/home']);
                }  
            }
            else {
                console.log("[navigationInterceptor] NavigationStart B",this.router.url.slice(1,3));
                console.log("[navigationInterceptor] NavigationStart C", this.auth.params);
                if ((!this.auth.params) || (!this.auth.params.token)) {
                    if (event.url != '/auth/forbiden')  {
                        this.router.navigate(['/auth/forbiden']);
                    }
                }
                else {
                    localStorage.clear();
                    this.auth.setSessionToken({token:this.auth.params.token}).then(
                        (response) => {
                            console.log('[navigationInterceptor] forbiden A',this.router);
                            localStorage.setItem('language', this.global.loggedUser().language);
                            localStorage.setItem('client', this.global.loggedUser().client_id);
                            let url = event.url.split('?')[0];
                            console.log('[navigationInterceptor] forbiden A', url);
                            if (url == '/') {
                                this.router.navigate(['/home']);
                            }                           
                        },
                        (error) => {
                            console.log('[AuthGuardService] forbiden A');
                            if (event.url != '/auth/forbiden') {
                                this.router.navigate(['/auth/forbiden']);
                            }
                        }
                    );                   
                }                        
            }
        }

        if (event instanceof NavigationEnd) {
            //this.blockUI.stop()
        }

        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof NavigationCancel) {
            //this.blockUI.stop()
        }

        if (event instanceof NavigationError) {
            //this.blockUI.stop()
        }
    }



    //   Apenas para fins de TESTE setar o client na mão
    setClientConfig(client: string): void {
        if (!CLIENT_DATA[client]) {
            console.error(
                "Cliente não encontrado. Verifique a chave utilizada e tente novamente"
            );
            return;
        }

        console.log(`Utilizando o usuário da ${client}`);
        console.table(CLIENT_DATA[client]);

        for (let [key, value] of Object.entries(CLIENT_DATA[client])) {
            sessionStorage.setItem(key, value.toString());
        }
    }
}
