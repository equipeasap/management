import { KnowledgeService } from './../../services/knowledge.service';
import { Global, AsapService } from 'asap-crud';
import { Component } from '@angular/core';

@Component({
  selector: 'app-box-cognition',
  templateUrl: './box-cognition.component.html',
  providers: [KnowledgeService]
})

export class BoxCognitionComponent {

    public user;
    public loading;
    public progress;

    constructor(
        public global: Global,
        public service: KnowledgeService        
    ) {
        this.user = this.global.loggedUser();
        //this.getContributors()
        this.getProgress();
    }

    public getProgress() {
        this.loading = true;
        this.service.getKnowledgeProgress(this.user.id).subscribe(
            (response) => {
                //console.log("getProgress",response);
                this.progress = response.progresso;
                console.log("getProgress",this.progress);
                // this.doSelectUser(response.data.dados[0]);
                this.loading = false;
            },
            error => {
                console.log('Error ->', error);
            }
        );
    }

    // public doSelectUser(user) {
    //     this.selectUser.emit(user);
    // }

}
