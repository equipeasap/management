import { Global } from 'asap-crud';
import { Component } from '@angular/core';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html'
})

export class NavbarComponent {

    public image;

    constructor(
        public global: Global,
        // public service: AsapService
    ) {
        // this.service.basePath = "api/v2/client";
        // this.service.entityName = "gamefication/pontos/cargo";
        // this.user = this.global.loggedUser();
        //this.getContributors()
        this.image = 'https://firebasestorage.googleapis.com/v0/b/admin-17e4f.appspot.com/o/images%2Flygtblklogo.png?alt=media&token=a860f37b-01ea-4908-94fc-7a176a9ceda0';
    }

    public logout() {
        console.log("rodou");
        window.close();
    }

}
