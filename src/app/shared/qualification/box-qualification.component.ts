import { AsapService, Global } from 'asap-crud';
import { Component } from '@angular/core';

@Component({
    selector: 'app-box-qualification',
    templateUrl: './box-qualification.component.html',
    providers:[AsapService]
})

export class BoxQualificationComponent {

    public user;

    constructor(
        public global: Global,
        public service: AsapService
    ) {
        this.service.basePath = "api/v2/client";
        this.service.entityName = "gamefication/pontos/cargo";
        this.user = this.global.loggedUser();
        //this.getContributors()
    }
}
