import { Component, Input } from '@angular/core';
import { Global, AsapService } from 'asap-crud';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { UtilsService } from 'src/app/services/utils.service';
import * as moment from 'moment';

@Component({
    selector: 'app-box-map',
    templateUrl: './box-map.component.html',
})

export class BoxMapComponent {

    @Input() userID:any;

    public user;
    public contributors;
    public notifications: any;
    public cargo;

    constructor(
        public global: Global,
        public service: AsapService,
        public db: AngularFirestore,
        public firebaseService: UtilsService
    ) {
        this.service.basePath = "api/v2/client/management";
        this.service.entityName = "feedback/top10";
        this.user = this.global.loggedUser();
        this.getUsersRanking();
        
        if (this.global.loggedUser()) {
            if (this.user.value.cargos == 'regional') {                
                this.cargo = false;
                console.log("cargo",this.cargo);
            }
            else {
                this.cargo = false;
                console.log("cargo",this.cargo);

            }
        }
    }

    ngOnInit() {
        this.getFeedbackNotifications();        
    }


    getUsersRanking() {
        this.service.getResources().subscribe(response => {
            console.log('response => aaa', response);
            this.contributors = response.data;
        })
    }

    public getFeedbackNotifications() {
        let notificationCollection: AngularFirestoreCollection;
        notificationCollection = this.firebaseService.db.collection('management/feedback/notifications', (ref: any) => {
            console.log("ref=",ref);

            if (this.userID) {       
                if (this.cargo) {
                    return ref.where('user.id', '==', [parseInt(this.user.id),parseInt(this.userID)])  
                              .where('message', '==', 'Competência respondida!')                                         
                              .orderBy('updated_at', 'desc').limit(15);
                } 
                else {
                    console.log("ids=",this.userID,this.user.id);

                    return ref.where('user.id', 'in', [parseInt(this.user.id),parseInt(this.userID)]) 
                              .where('message', '==', 'Competência respondida!')
                              //.where('updated_at', '>=', moment.format())                                                     
                              .orderBy('updated_at', 'desc').limit(15);
                }                              
                
            } 
            else {                
                let date = new Date('2020-05-01');
                let newDate = date.getTime();
                console.log(newDate);
                return ref.where('updated_at', '>=', newDate)   
                          .orderBy('updated_at', 'desc').limit(15);
            }
           
        });


        this.notifications = notificationCollection.valueChanges();//.subscribe(values => {                
        //   console.log(values);
        //   this.notifications = [];
        //   values.map(document => {
        //     const data = document.payload.doc.data();
        //     console.log(data);
        //     this.notifications.push(data);
        //   });
        //})
    }

}
