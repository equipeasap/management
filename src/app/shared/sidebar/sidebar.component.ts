
import { Component } from '@angular/core';

declare const $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})

export class SidebarComponent  {

  public menus = [
    { type: 'skills', text: 'Competências', icon: 'fa fa-lightbulb-o'},
    { type: 'map', text: 'Mapa de conhecimentos', icon: 'fa fa-map-o'},
    { type: 'pfa', text: 'PFA', icon: 'fa fa-question-circle-o'},
  ];

  ngOnInit() {

    $(document).ready(function () {

      $('.sidebarCollapse, .sidebar-menu').on('click', function () {
        $('#sidebar, .sidebar-menu').toggleClass('active');
      });

      $(document).on('click', '#btn-h', function() {
        $(this).attr("id","btn-v").html("<i class='fa fa-arrow-right'></i>");
      });

      $(document).on('click', '#btn-v', function() {
        $(this).attr("id","btn-h").html("<i class='fa fa-arrow-left'></i>");
      });
    });
  }

}
