import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { OwlModule } from 'ngx-owl-carousel';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BoxContributorsComponent } from './contributors/box-contributors.component';
import { BoxSkillsComponent } from './skills/box-skills.component';
import { BoxCognitionComponent } from './cognition/box-cognition.component';
import { BoxPfaComponent } from './pfa/box-pfa.component';
import { BoxQualificationComponent } from './qualification/box-qualification.component';
import { BoxMapComponent } from './map/box-map.component';
import { AsapLimitToModule } from 'asap-limit-to';
import { AsapSkeletonModule } from 'asap-skeleton';
import { AvatarModule } from 'ngx-avatar';
import { AsapSharedModule } from 'asap-shared';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { TranslateSharedModule } from '../core/translate/translate.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AsapLimitToModule,
    AsapSkeletonModule,
    OwlModule,
    NgxSkeletonLoaderModule,
    AvatarModule,
    AsapSharedModule,
    TranslateSharedModule
  ],
  declarations: [
    NavbarComponent,
    SidebarComponent,
    BoxContributorsComponent,
    BoxSkillsComponent,
    BoxCognitionComponent,
    BoxPfaComponent,
    BoxQualificationComponent,
    BoxMapComponent
  ],
  exports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    OwlModule,
    TranslateSharedModule,
    NavbarComponent,
    SidebarComponent,
    BoxContributorsComponent,
    BoxSkillsComponent,
    BoxCognitionComponent,
    BoxPfaComponent,
    BoxQualificationComponent,
    BoxMapComponent,
    NgxSkeletonLoaderModule,
  ]
})
export class SharedModule {
  
}
