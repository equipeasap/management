import { KnowledgeService } from './../../services/knowledge.service';
import { Global } from 'asap-crud';
import { Component } from '@angular/core';

@Component({
  selector: 'app-box-pfa',
  templateUrl: './box-pfa.component.html',
})

export class BoxPfaComponent {

  public user;
  public loading;
  public progress;

  constructor(
      public global: Global,
      public service: KnowledgeService        
  ) {
      this.user = this.global.loggedUser();      
      this.getProgress();
  }

  public getProgress() {
      this.loading = true;
      this.service.getKnowledgeProgress(this.user.id).subscribe(
          (response) => {
              this.progress = response.progresso;              
              console.log("getProgress 2",this.progress);
              this.loading = false;
          },
          error => {
              console.log('Error ->', error);
          }
      );
  }
}
