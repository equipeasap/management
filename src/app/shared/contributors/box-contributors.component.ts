import { Component, Output, EventEmitter } from '@angular/core';
import { Global, AsapService } from 'asap-crud';

@Component({
    selector: 'app-box-contributors',
    templateUrl: './box-contributors.component.html',
    providers:[AsapService]
})
export class BoxContributorsComponent {

    public contributors;

    @Output() selectUser = new EventEmitter<any>();

    public user;
    public loading;

    constructor(
        public global: Global,
        public service: AsapService
    ) {
        this.service.basePath = "api/v2/client";
        this.service.entityName = "gamefication/pontos/cargo";
        this.user = this.global.loggedUser();
        this.getContributors()
    }

    public getContributors() {
        this.loading = true;
        this.service.getResource(this.user.id).subscribe(
            (response) => {
                console.log("getContributors",response);
                this.contributors = response.data.dados;
                this.doSelectUser(response.data.dados[0]);
                this.loading = false;
            },
            error => {
                console.log('Error ->', error);
            }
        );
    }

    public doSelectUser(user) {
        this.selectUser.emit(user);
    }

}
