import { NgModule } from '@angular/core';
import { AngularFireModule, FirebaseOptionsToken } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AsapClientPipe } from './core/pipes/asap-client.pipe';
import { ThemeService } from './services/theme.service';

export function firebaseData() {
    console.log('[firebaseData]', new Date(), AsapClientPipe.dictionary.firebase);
    return AsapClientPipe.dictionary.firebase;
}

@NgModule({
    imports: [
        AngularFireModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
    ],
    declarations: [

    ],
    // providers: [
    //     { provide: FirebaseOptionsToken, useValue: firebaseData() },
    //     ThemeService
    // ],
    exports: [
        AngularFireModule,
        AngularFirestoreModule,
        AngularFireStorageModule
    ]
})
export class FirebaseModule {

    constructor() {
        console.log('[FirebaseModule]', new Date());
        console.log('[FirebaseModule] FireBase Data', firebaseData());
    }
}

