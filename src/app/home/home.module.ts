import { AvatarModule } from 'ngx-avatar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';

import { HomeRoutes } from './home.routing';

import { HomeComponent } from './home.component';
import { AsapSkeletonModule } from 'asap-skeleton';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(HomeRoutes),
    SharedModule,
    AsapSkeletonModule,
    AvatarModule
  ],
  declarations: [
    HomeComponent
  ],
  exports: [
    SharedModule
  ]
})
export class HomeModule { }
