import { KnowledgeService } from './../services/knowledge.service';
import { ActivatedRoute } from '@angular/router';
import { CompetenceSurveyService } from './../services/competence-survey.service';
import { CompetenceRatingService } from './../services/competence-rating.service';
import { Component, ViewChild } from '@angular/core';
import { Global, AsapService } from 'asap-crud';
import { OwlCarousel } from 'ngx-owl-carousel';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss', '../dashboard/dashboard.component.scss'],
    providers: [AsapService, CompetenceRatingService, CompetenceSurveyService, KnowledgeService]
})

export class HomeComponent {


    public viewPeriodPfa = 'currentYearPfa';
    public viewPeriod = 'currentYear';

    public pfa;
    public view;

    public pfas = [
        { name: '4º Trimestre', date_start: '2020-10-07', date_end: '2020-11-30', pendencias: 1, color: 'text-success' }
        // { status: 'Você possui 12 pendências.', q: 'Q2', months: 'Abril, Maio e Junho', color: 'text-warning' },
        // { status: 'Fora do prazo, aguarde!', q: 'Q3', months: 'Julho, Agosto e Setembro' },
        // { status: 'Fora do prazo, aguarde!', q: 'Q4', months: 'Outubro, Novembro e Dezembro' },
    ];

    @ViewChild('owlPfa', { static: false }) owlPfa: OwlCarousel;

    options = {
        center: false,
        loop: false,
        margin: 15,
        rewind: true,
        autoplay: false,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: false,
        nav: false,

        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            1024: {
                items: 3,
            }
        }
    };

    public filter = 'users';
    public user;
    public users;
    public cargo = true;
    public dateNow;
    public colaboradores;
    public array = [];
    public competence;
    public teste;
    public ratingAvaliador;
    public ratingAvaliado;
    public knowledge;
    public loading = true;
    public loadAdmin = true;
    public loadNormal = true;
    public avalia360Dados;
    public loading360;
    public cargoComp;
    public rating;
    public evaluatedData;

    public menus = [
        { type: 'skills', text: 'Competências', icon: 'fa fa-lightbulb-o' },
        { type: 'map', text: 'Últimos Feedbacks', icon: 'fa fa-map-o' },
        { type: 'pfa', text: 'PFA', icon: 'fa fa-question-circle-o' },
    ];

    constructor(
        public global: Global,
        public service: AsapService,
        public route: ActivatedRoute,
        public ratingService: CompetenceRatingService,
        public surveyService: CompetenceSurveyService,
        public knowledgeService: KnowledgeService
    ) {

        this.service.basePath = 'api/v2/client';
        this.service.entityName = 'management';
        this.user = this.global.loggedUser();
        console.log(this.user);
        this.getUsers(this.user.id);

        if (this.user.value.cargos == 'lider' || this.user.value.cargos == 'regional') {
            this.cargoComp = true;
        } else {
            this.cargoComp = false;
        }

        this.ratingService.getRating(this.user.id).subscribe(response => {
            this.rating = response.data;
            this.evaluatedData = response.evaluatedData;
            // console.log("rating Avaliador 2-> ", this.evaluatedData[0].id);

        });


        // this.service.getResources().subscribe(response => {
        //     console.log("xd ->", response);
        // });

        // if (this.view == 'Gestor') {
        //     console.log("gestooor");
        // }
        // else {
        //     console.log("outrooo");
        // }

        if (this.global.loggedUser()) {
            console.log(this.user.id);
            if (this.cargo) {
                //this.cargo = false;
                this.loading = true;
                this.ratingService.getRating().subscribe(response => {
                    this.ratingAvaliador = response.data;
                    console.log("rating Avaliador -> ", this.ratingAvaliador);
                    this.loading = false;
                });
            }
            else {
                //this.cargo = true;
                this.loading = true;
                this.ratingService.getRating(this.user.id).subscribe(response => {
                    this.ratingAvaliado = response.data;
                    console.log("rating Avaliado  -> ", this.ratingAvaliado);
                    this.loading = false;
                });
                this.knowledgeService.getKnowledgeByUser(this.user.id).subscribe(response => {
                    this.knowledge = response;
                    console.log("cursos",this.knowledge);
                    this.loading = false;
                    this.loadNormal = false;
                });
            }
        }

        this.getAvalia360Data();

    }

    public getUsers(id) {
        this.loadAdmin = true;
        this.service.getResource(id).subscribe(response => {
            // console.log('response -> xcd', response.data);
            this.users = response.data;
            if (this.users.lider) {
                this.cargo = true;
                if (this.users.dados) {
                    this.users.dados = this.users.dados.map(item => {
                        if ((item.user.value) && (item.user.value != '')) {
                            item.user.value = JSON.parse(item.user.value);
                        }

                        if ((item.conhecimento) && (item.conhecimento != '')) {
                            item.pendencias = item.conhecimento.filter(i => {
                                if ((i.resp == 'Falta prática') || (i.resp == 'Preciso de suporte')) {
                                    return true;
                                }
                                return false;
                            });
                        }

                        return item;
                    });
                }
                this.loadAdmin = false;
                if (this.users.dadosPFA) {
                    // this.users.dadosPFA.status = 0;
                    this.users.dadosPFA.forEach(element => {
                        if (element.status == 0) {
                            element.statusName = 'Pendente';
                            element.badge = 'warning';
                        } else {
                            element.statusName = 'Finalizado';
                            element.badge = 'success';
                        }
                    });

                }
            }
        });
    }

    public selectView(view) {
        console.log('view', view.value);
        if (view.value == 'Gestor') {
            this.cargo = true;
        } else {
            this.cargo = false;
        }
    }

    public getAvalia360Data() {
        this.loading360 = true;
        this.service.basePath = 'api/v2/admin';
        this.service.entityName = 'feedback/360';

        this.service.getResources().subscribe(response => {
            this.avalia360Dados = response.data;
            // console.log('360',this.avalia360Dados);
            this.avalia360Dados = this.avalia360Dados.map(item => {
                // item.status = 0;
                if (item.status == 0) {
                    item.statusName = 'Pendente';
                    item.badge = 'warning';
                } else {
                    item.statusName = 'Avaliado';
                    item.badge = 'success';
                }

                return item;

            });

            this.loading360 = false;
        });
    }

    // public rating = [
    //     { description: '1 Trimestre', date_end: '01/03/2020', id: 1 },
    //     { description: '2 Trimestre', date_end: '01/11/2020', id: 2 },
    //     { description: '3 Trimestre', date_end: '01/11/2021', id: 3 },
    // ];


    // public users = [
    //   { avatar: 'assets/img/edu.jpg', name: 'Eduardo Ramos Strucchi', ranking: '1º', points: '50', individual_courses: '3', total_courses: '5', evaluation: '4.33', time: '00:15:00' },
    //   { avatar: 'assets/img/herber.jpg', name: 'Herber Jackson', ranking: '2º', points: '50', individual_courses: '3', total_courses: '5', evaluation: '4.33', time: '00:15:00' },
    // ];

}
