import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SafeUrlPipe } from './safe-url.pipe';
import { StreamPipe } from './stream.pipe';
import { AsapClientPipe } from './asap-client.pipe';
import { orderByPipe } from './order-by.pipe';
import { TextTruncatePipe } from './text-truncate.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    SafeUrlPipe,
    StreamPipe,
    AsapClientPipe,
    orderByPipe,
    TextTruncatePipe
  ],
  exports: [
    SafeUrlPipe,
    StreamPipe,
    AsapClientPipe,
    orderByPipe,
    TextTruncatePipe
  ]
})

export class PipesModule { }
