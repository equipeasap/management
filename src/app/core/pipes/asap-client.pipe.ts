import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'asapClient'
})
export class AsapClientPipe implements PipeTransform {

    public static dictionary:any = {
        appName: 'Portal Smart Fit',
        favicon: 'assets/img/emblem-small.png',
        clientName: 'Smart Fit',
        welcome: 'Bem vindo',
        intro: 'Você está na Universidade Smart Fit',
        environmentWelcomeParagraph: 'Nesta página você terá acesso aos conteúdos e diretrizes que serão apresentados aos novos colaboradores do Smart Fit',
        influencersParagraph: 'Você também pode ser um influenciador do Grupo Bio Ritmo/Smart Fit',
        loginBG: 'https://res.cloudinary.com/midia9-group/image/upload/v1562336393/image55399_anpfnb.png',
        logoLight: 'assets/img/logo-portal.png',
        logoDark: 'assets/img/logo_portal_black.png',
        logoFooter: 'assets/img/logo-footer.png',
        logohelpdesk: 'assets/img/helpdesk.png',
        logoEAD: 'assets/img/logo.png',
        emblem: 'assets/img/emblem.png',
        bannerHome: 'assets/img/banner-home.jpg',
        bannerPage: 'assets/img/banner-home.jpg',
        bannerVoucher: 'assets/img/banner-home-beneficios.jpg',
        mainColor: '#f4ac1e',
        bgClient: 'white',
        bgSidebar: '#1f2123',
        linkSidebar: 'white',
        bgNavbar: '#1f2123',
        linkNavbar: 'white',
        bgFooter: '#1f2123',
        linkFooter: 'white',
        bgBtn: '#f4ac1e',
        textBtn: 'white',
        bgSidebarViewer: 'white',
        bgBtnViewer: '#f4ac1e',
        textBtnViewer: 'white',
        textBtnViewerActive: '#f4ac1e',
        primaryColor: '#f4ac1e',
        primaryColorHover: '#f4ac1e',
        supplementaryDataRequired: false,
        loginAD: false,
        firebase: null,
        documentsLayout: ''
    };

    public static setDictionary(dic) {
        for (const key in dic) {
            AsapClientPipe.dictionary[key] = dic[key];
        }
    }

    transform(value: any, args?: any): any {
        if (AsapClientPipe.dictionary[value] !== undefined) {
            return AsapClientPipe.dictionary[value];
        }
        return value;
    }

}
