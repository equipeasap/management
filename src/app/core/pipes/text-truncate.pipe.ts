import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'textTruncate'})
export class TextTruncatePipe implements PipeTransform {
    transform(text: any, length: any): any {
        return (text.length > length) ? text.slice(0, length) + '...' : text;

    }
}
