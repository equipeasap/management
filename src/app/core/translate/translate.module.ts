import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            },
            isolate: false
        })
    ],
    exports: [
        TranslateModule
    ],
    providers: [ TranslateService ]
})
export class TranslateSharedModule {

    constructor(translate: TranslateService) {
        let language = localStorage.getItem('language') || 'pt-br';

        if (language == 'undefined') {
            language = 'pt-br';
        }
        
        console.log('[TranslateSharedModule] Language', language);
        translate.setDefaultLang(language);
        translate.use(language);
    }

 }
