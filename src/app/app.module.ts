import { LogoutComponent } from './logout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { ForbidenComponent } from './auth-guard/forbiden.component';
import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AsapCrudModule } from 'asap-crud';
import { AsapLimitToModule } from 'asap-limit-to';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { registerLocaleData } from '@angular/common';
import localeBr from '@angular/common/locales/pt';
import { AngularFireModule } from '@angular/fire';
import { Ng2IziToastModule } from 'ng2-izitoast';
import { FirebaseModule } from './firebase.module';
import { BlockUIModule } from 'ng-block-ui';
import { AsapSharedModule } from 'asap-shared';
import { SharedModule } from './shared/shared.module';

registerLocaleData(localeBr, 'pt-BR');

@NgModule({
    imports: [
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        RouterModule.forRoot(AppRoutes),
        // Imports Externos
        AsapCrudModule,
        Ng2IziToastModule,
        AsapLimitToModule,
        BlockUIModule.forRoot(),
        SimpleNotificationsModule.forRoot(),
        FirebaseModule,
        AngularFireModule.initializeApp({
            apiKey: "AIzaSyDcVaYzLKZKPFwL31F099rp3p8s30XYlQ0",
            authDomain: "universidade-smart-fit.firebaseapp.com",
            databaseURL: "https://universidade-smart-fit.firebaseio.com",
            projectId: "universidade-smart-fit",
            storageBucket: "universidade-smart-fit.appspot.com",
            messagingSenderId: "706681009423"
        }),
        AsapSharedModule,
        SharedModule
    ],
    declarations: [
        AppComponent,
        DashboardComponent
    ],
    exports: [
        AsapLimitToModule,
        Ng2IziToastModule
    ],
    providers: [
        AuthService,
        AuthGuardService,        
        { provide: LOCALE_ID, useValue: "pt-BR" }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
