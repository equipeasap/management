
# Estagio 1 - Será responsavel em construir nossa aplicação
FROM scratch as blank
WORKDIR /app
COPY ./dist /app/

# Estagio 2 - Será responsavel por expor a aplicação
FROM nginx:1.13
COPY --from=blank /app /usr/share/nginx/html
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf